import React, { useContext } from 'react';
import PlatillosPageListAIn from '../pages/platillos';
import PlatillosPageAgregar from '../pages/platillosAgregar';
import PlatillosPageListAc from '../pages/platillosListaAct';
import ReservacionPage from '../pages/reservacion';
import UsuarioPage from '../pages/usuario';
import VistaComandasPage from '../pages/ver_comandas';
import ComandasPage from '../pages/comandas';
import EmpleadosPage from '../pages/empleado';
import LoginPage from '../pages/loginPage';
import MesasPage from '../pages/mesas';
import PagossPage from '../pages/pagos';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Vista from '../components/Vista';


const RoutersAdmin = () => {
    return(
       <Router>
            <Vista />
            
       </Router>
    );
}
export default RoutersAdmin;