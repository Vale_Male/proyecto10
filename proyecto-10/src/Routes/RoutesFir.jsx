import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Vista from '../components/Vista';
import ComandasPage from '../pages/comandas';
import EmpleadosPage from '../pages/empleado';
import LoginPage from '../pages/loginPage';
import MesasPage from '../pages/mesas';
import PagossPage from '../pages/pagos';
import PlatillosPageListAIn from '../pages/platillos';
import PlatillosPageAgregar from '../pages/platillosAgregar';
import PlatillosPageListAc from '../pages/platillosListaAct';
import ReservacionPage from '../pages/reservacion';
import UsuarioPage from '../pages/usuario';
import VistaComandasPage from '../pages/ver_comandas';
import RoutersAdmin from './RouterAdmin';


const Routers = () => {
    return(
       <Router>

            <Switch>
            <Route exact path="/vista" component={Vista} />
            {/* Rutas de dashboard */}
            <Route exact path="/adminDashboard" component={RoutersAdmin} />
            {/* Rutas de los platillos */}
            <Route exact path="/platillosAgregar" component={PlatillosPageAgregar} />
            <Route exact path="/platillosListaAc" component={PlatillosPageListAc} />
            <Route exact path="/platillosListaIn" component={PlatillosPageListAIn} />
            {/* Rutas */}

                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/usuarios" component={UsuarioPage} />
                <Route exact path="/empleados"  component={EmpleadosPage}/>
                
                <Route exact path="/mesas" component={MesasPage}/>
                <Route exact path="/reservacion" component={ReservacionPage}/>
                <Route exact path="/comandas" component={ComandasPage}/>
                <Route exact path="/ver_comandas" component={VistaComandasPage}/>
                <Route exact path="/pagos" component={PagossPage}/>
                <Route exact path="/character/:id" />
                <Redirect to="/login" />
            </Switch>
       </Router>
    );
}
export default Routers;