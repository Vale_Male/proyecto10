
import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route,NavLink,Link,useHistory } from 'react-router-dom';

import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import {
  CDBSidebarMenuItem,
} from 'cdbreact';
import EmpleadosPage from '../pages/empleado';
import MesasPage from '../pages/mesas';
import PlatillosPage from '../pages/platillos';
import ReservacionPage from '../pages/reservacion';
import ComandasPage from '../pages/comandas';
import { Helmet } from 'react-helmet';

// Be sure to include styles at some point, probably during your bootstraping


const NavbarRoute = () => {
  const history = useHistory();

    const handleUsuarios = () =>{
      history.push("/usuarios");
    }
    const handleEmpleados = () =>{
        history.push("/empleados");
    }
    const handlePlatillos = () =>{
        history.push("/platillosAgregar");
    }
    const handleMesas = () =>{
        history.push("/mesas");
    }
    const handleReservacion = () =>{
        history.push("/reservacion");
    }
    const handleComandas = () =>{
        history.push("/comandas");
    }
    const handleVerComandas = () =>{
        history.push("/ver_comandas");
    }
    const handlePagos = () =>{
        history.push("/pagos");
    }
      return (
        <Router>
             <Helmet>
             <style>{"body{background-image: /assets/fondo.jpg;background-repeat: no-repeat;width=100%;height=100%;}"}</style>
            </Helmet>
          <nav className="navbar navbar-expand-lg navbar-light bg-green-dark margin-nav">
          <div className="container-fluid">
              <img src="/assets/logo.png"  width="150px" height="50"/>

              <button className="navbar-toggler text-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon text-light"  />
              </button>
              <div className="collapse navbar-collapse justify-content-right" id="navbarNav">
                  <ul className="navbar-nav me-auto mb-2 mb-lg-0 justify-content-right">
                      
                  </ul>
                  
                  <div classNmae="d-flex">
                      <button className="btn active text-light fs-5 mb-2 text-ligth text-wrap" onClick="" >
                          Usuario
                      </button>
                      <button className="btn  text-ligth text-wrap" onClick="" >
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-box-arrow-in-right text-wrap text-light" color="ffffff" viewBox="0 0 16 16">
                              <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/>
                              <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                          </svg>
                      </button>
                  </div>
              </div>

          </div>

          </nav>
          <SideNav className="bg-purple" style={{backgroundColor: '#5BD959'}}>
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected="home">
                  
                    <NavItem eventkey={7}  onClick={handleUsuarios}>
                        <NavIcon >
                            <i className="fa fa-fw fa-user" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Usuarios
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={1} onClick={handleEmpleados} >
                        <NavIcon >
                            <i className="fa fa-fw fa-user-circle" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Empleados
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={2}  onClick={handlePlatillos} >
                        <NavIcon >
                            <i className="fa fa-fw fa-utensils" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Platillos
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={3} onClick={handleMesas}  >
                        <NavIcon >
                            <i className="fa fa-fw fa-stroopwafel" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Mesas
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={4} onClick={handleReservacion}  >
                        <NavIcon >
                            <i className="fa fa-fw fa-book-open" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Reservación
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={5} onClick={handleComandas}  >
                        <NavIcon >
                            <i className="fa fa-fw fa-cocktail" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Comandas
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={6} onClick={handleVerComandas}  >
                        <NavIcon >
                            <i className="fa fa-fw fa-cookie-bite" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Ver Comandas
                        </NavText>
                    </NavItem>
                    <NavItem eventkey={6} onClick={handlePagos}  >
                        <NavIcon >
                            <i className="fa fa-fw fa-money-bill-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </NavIcon>
                        <NavText className="Text-light" style={{ fontSize: '1.75em', color: '#ffffff' }} >
                        Pagos
                        </NavText>
                    </NavItem>
                </SideNav.Nav>
            </SideNav>
            <Switch>
               
            </Switch>
        </Router>
       

  );
};
    export default NavbarRoute;