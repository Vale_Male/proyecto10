
import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route,Link,useHistory } from 'react-router-dom';

import { Helmet } from 'react-helmet';
import {
    UncontrolledCollapse,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    NavbarBrand,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container
  } from "reactstrap";
import Modelo from '../pages/modelo';
  
  // core components
  
// Be sure to include styles at some point, probably during your bootstraping


const Vista = () => {
    const [bodyClick, setBodyClick] = React.useState(false);
    const history = useHistory();

    const handleUsuarios = () =>{
      history.push("/usuarios");
    }
    const handleEmpleados = () =>{
        history.push("/empleados");
    }
    /** Rutas platillos */
    const handlePlatillosAgregar = () =>{
      history.push("/platillosAgregar");
    }
    const handlePlatillosAt = () =>{
        history.push("/platillosListaAc");
    }
    const handlePlatillosIn = () =>{
        history.push("/platillosListaIn");
    }
    /** Rutas platillos */

      const handleMesas = () =>{
        history.push("/mesas");
    }
    const handleReservacion = () =>{
        history.push("/reservacion");
    }
    const handleComandas = () =>{
        history.push("/comandas");
    }
    const handleVerComandas = () =>{
        history.push("/ver_comandas");
    }
    const handlePagos = () =>{
        history.push("/pagos");
    }
  return (
    <>
    <Helmet>
             <style>{"body{background:#EEEEEE;}"}</style>
            </Helmet>
      {bodyClick ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setBodyClick(false);
          }}
        />
      ) : null}
        <Navbar color="fixed-top navbar-light bg-success" expand="lg">
          <Container>
            <NavbarBrand  className="text-ligth" href="#pablo" onClick={e => e.preventDefault()}>
              <img src="/assets/logo.png"  width="120" height="40"/>
            </NavbarBrand>
            <button
              className="navbar-toggler text-ligth"
              id="navbarTogglerDemo02"
              type="button"
              onClick={() => {
                document.documentElement.classList.toggle("nav-open");
                setBodyClick(true);
              }}
            >
              <span className="navbar-toggler-bar bar1" />
              <span className="navbar-toggler-bar bar2" />
              <span className="navbar-toggler-bar bar3" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbarTogglerDemo02" color={"sucess"}>
              <Nav className="mr-auto mt-2 mt-lg-0 " navbar >
                <NavItem className="active">
                  <NavLink href="" onClick={e => e.preventDefault()}>
                    Home <span className="sr-only">(current)</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="" onClick={e => e.preventDefault()}>
                    Link
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className="disabled"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    Disabled
                  </NavLink>
                </NavItem>
                <UncontrolledDropdown nav>
                <DropdownToggle
                  aria-haspopup={true}
                  caret
                  color="default"
                  data-toggle="dropdown"
                  href="#pablo"
                  id="navbarDropdownMenuLink"
                  nav
                  onClick={e => e.preventDefault()}
                >
                 Platillos
                </DropdownToggle>
                <DropdownMenu aria-labelledby="navbarDropdownMenuLink">
                  <DropdownItem
                    onClick={handlePlatillosAgregar}
                  >
                    Agregar
                  </DropdownItem>
                  <DropdownItem
                    onClick={handlePlatillosAt}
                  >
                    Platillos activos
                  </DropdownItem>
                  <DropdownItem
                    onClick={handlePlatillosIn}
                  >
                    Platillos inactivos
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              </Nav>
             
            </UncontrolledCollapse>
          </Container>
        </Navbar>
    </>
  );
}
  
    export default Vista;