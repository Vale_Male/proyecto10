import React, {useState} from 'react'
import DataTable from 'react-data-table-component';
import MyComponent from '../components/table';
import {Button,  Modal,Container,Row,Col }  from 'react-bootstrap';
import NavbarRoute from '../components/NavbarRoute';

const EmpleadosPage = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [showEli, setShoEliw] = useState(false);

    const handleCloseEli = () => setShoEliw(false);
    const handleShowEli = () => setShoEliw(true);

    const [Editshow, setShoEditww] = useState(false);

    const handleCloseEdite = () => setShoEditww(false);
    const handleShowEdite = () => setShoEditww(true);
    return (
        <>
                <NavbarRoute />

        <div>
        <div className="container text-center col-md-11">

            <div className="card col-md-12  row justify-content-center m-3 mt-5">
                <h1>Empleados</h1>
            </div>
           
        </div>
        <div className="container  col-md-11">

            <div className=" col-md-12  row  m-3 ">
                <Button variant="primary btn btn-primary col-md-1" onClick={handleShow}>
                
                    <i className="fa fa-fw fa-user-plus" style={{ fontSize: '1.75em', color: '#ffffff' }} />

                </Button>
            </div>
             
        
        </div>
        <div className="container  col-md-11">
            <div className="container  mt-5 col-md-11">
            <table className="table">
            <thead className="bg-gray">
                <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Tipo de empleado</th>
                <th scope="col">Estatus</th>
                <th scope="col">Acciónes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th >1</th>
                <td>Mark</td>
                <td>Admin</td>
                <td>Activo</td>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example"  >
                        <button type="button" className="btn btn-info" onClick={handleShowEdite} >
                            <i className="fa fa-fw fa-user-edit" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                        <button type="button" className="btn btn-danger" onClick={handleShowEli}>
                            <i className="fa fa-fw fa-trash-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>
                </tr>
                <tr>
                <th >Item 1</th>
                <td>Item 1</td>
                <td>Item 1</td>
                <td>Activo</td>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example" >
                        <button type="button" className="btn btn-info" onClick={handleShowEdite}>
                            <i className="fa fa-fw fa-user-edit" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                        <button type="button" className="btn btn-danger" onClick={handleShowEli}>
                            <i className="fa fa-fw fa-trash-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>
                </tr>
                <tr>
                <th >Item 1</th>
                <td>Item 1 </td>
                <td>Item 1</td>
                <td>Activo</td>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example" >
                        <button type="button" className="btn btn-info" onClick={handleShowEdite}>
                            <i className="fa fa-fw fa-user-edit" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                        <button type="button" className="btn btn-danger" onClick={handleShowEli}>
                            <i className="fa fa-fw fa-trash-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>

                
                </tr>
            </tbody>
            </table>
            </div>
        </div>
    </div>
    <Modal show={show} onHide={handleClose} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Agregar empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
        <form>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">Nombre</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">Teléfono</label>
            <input type="password" className="form-control" id="exampleInputPassword1" />
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">Tipo</label>
            <input type="password" className="form-control" id="exampleInputPassword1" />
            </div>
      </form>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="primary" onClick={handleClose}>
            Guardar
          </Button>
          <Button variant="secondary" className="btn btn-danger"onClick={handleClose}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>
    
    <Modal show={showEli} onHide={handleCloseEli} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Eliminar empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
            <h4>Seguro que desea eliminar el empleado?</h4>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleCloseEli}>
            Si
          </Button>
          <Button variant="secondary" className="btn btn-danger" onClick={handleCloseEli}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>

    <Modal show={Editshow} onHide={handleCloseEdite} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Modificar empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
        <form>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">Nombre</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">Teléfono</label>
            <input type="password" className="form-control" id="exampleInputPassword1" />
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">Tipo</label>
            <input type="password" className="form-control" id="exampleInputPassword1" />
            </div>
      </form>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="primary" onClick={handleCloseEdite}>
            Guardar
          </Button>
          <Button variant="secondary" className="btn btn-danger"onClick={handleCloseEdite}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>
    </>
    )
}

export default EmpleadosPage;