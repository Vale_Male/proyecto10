import React, {useState, useContext} from 'react'
import DataTable from 'react-data-table-component';
import MyComponent from '../components/table';
import {Button,  Modal,Container,Row,Col }  from 'react-bootstrap';
import { BrowserRouter as Router, useHistory } from 'react-router-dom';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import {Helmet} from 'react-helmet';
import axios from 'axios';



/**
* Estamos llegando aquí a partir del LoginRouter y este es mi router principal por lo que el history lo podemos recibir 
como un prop, para ello hay que desestructurarlo.
* @param {*} history 
* @returns 
*/

const LoginPage = () => {
    
  //Recuperamos el context y el dispatch para poderlo modificar.
  
  const history = useHistory();
    

    const [data, setData] = useState({ usuario: "", contrasenia: ""});
    const {usuario, contrasenia} = data;

    // Definimos la función handleChange
    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value,
        })
    }


    const handleLogin = async () => {
      const formData = new FormData();
      formData.append('usuario', usuario);
      formData.append('contrasenia', contrasenia);
      console.log(usuario + " a " + contrasenia);
     
      axios({
            method: "post",
            url:"https://apis.pegasoirapuato.com/public/api/v1/login" ,
            data: formData,
            headers: { "Content-Type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer",
            "type": "formData"
        },
          })
            .then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data.usuario);
              //Usamos el dispatch para modificar el context y establecerlo en true.
              //dispatch({type: authTypes.login});
              localStorage.setItem('usuario', JSON.stringify(response.data.usuario));
              history.push("/vista");
            })
            .catch(function (response) {
              //handle error
              alert("Usuario o contraseña incorrectos");
      });
    }
    
    const handleUsuarios = () =>{
      history.push("/vista");
    }
    return (
    <>
    <Helmet>
    <style>{"body {background-image: url('/assets/moyuelo-lugar.jpg');background-size: cover;background-repeat: no-repeat;height: 100%;font-family: 'Numans', sans-serif;}"}</style>
    <link href="css/styleLogin.css" rel="stylesheet" />

    </Helmet>
    <div className="container">
        <div className="d-flex justify-content-center h-100">
          <div className="card">
            <div className="card-header mt-5">
              <h3>Sign In</h3>
              
            </div>
            <div className="card-body">
              <form>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-user" /></span>
                  </div>
                  <input type="text" className="form-control" placeholder="Username" name="usuario"  onChange={handleChange}/>
                </div>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-key" /></span>
                  </div>
                  <input type="password" className="form-control" placeholder="Password" name="contrasenia" onChange={handleChange}/>
                </div>
                <div className="row align-items-center remember">
                  <input type="checkbox" className="text-color-l" />Remember Me
                </div>
                <div className="form-group">
                <NavItem eventkey={6} onClick={handleLogin}   className="btn float-right login_btn">

                        <NavText className="Text-light" >
                        Inicio
                        </NavText>
                    </NavItem>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
    )
}

export default LoginPage;