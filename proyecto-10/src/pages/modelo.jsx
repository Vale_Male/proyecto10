
import React, {useState,useEffect,useCallback} from 'react'
import DataTable from 'react-data-table-component';
import axios from 'axios';
import MyComponent from '../components/table';
import { useHistory } from 'react-router-dom';
import {Button,  Modal,Container,Row,Col, NavLink,Alert }  from 'react-bootstrap';
import platillos from './services/platillos/servicePlatillo';
import platillosAdd from './services/platillos/servicePlatilloAdd';
import platillosUpdate from './services/platillos/servicePlatilloUpdate';
import platillosDescativate from './services/platillos/servicePlatilloDesactivate';

import NavbarRoute from '../components/NavbarRoute';
const Modelo = () => {
    //Listas
    
    const [ listPlatillos, setListCustomer ] = useState([]);
    useEffect(()=>{
      async function fetchDataCustomer(){
        const res = await platillos.list();
        console.log(res.data);
        setListCustomer(res.data)
      }
      fetchDataCustomer();
    },[])
  
    //Agregar 
    const [name, setName] = useState("");
    const [precio, setPrecio] = useState("");
    const [image, setImage] = useState(null);
  
    const onClickSave = async () => {
      if(image&& name && precio){
        setShow(false)
      const datapost = { name, image, precio}
  
      const res = await platillosAdd.save(datapost);
  
      if (res) {
        alert("Se agrego");
        window.location.href = "/platillos";
  
      }
      else {
        alert("Se agrego");
        window.location.href = "/platillos";
      }
      }else{
        alert("Escriba los valores")
      }
      
    }
   //Modificar 
   const [nameUp, setNameUp] = useState("");
   const [precioUp, setPreciop] = useState("");
   const [imageUp, setImagep] = useState(null);
   const [idUp, setIdUp] = useState(null);
  
   const onClickUpdateDates = async (data) => {
    console.log(data);
    setNameUp(data.nombre);
    setPreciop(data.precio);
    setImagep(data.image);
    setIdUp(data.id);
    
      handleShowEdit();
    }
    const onClickSaveDates = async () => {
      if(nameUp && precioUp){
        setShoEdit(false)
      const datapost = { nameUp, imageUp, precioUp,idUp}
  
      const res = await platillosUpdate.update(datapost);
  
      if (res) {
        alert("Se modifico");
        //window.location.href = "/platillos";
  
      }
      else {
        alert("Se modifico");
       //window.location.href = "/platillos";
      }
      }else{
        alert("Escriba los valores")
      }
      
    }
    //Desactivar 
    const [idUpDel, setIdUpDel] = useState("");
    const [idP, setIdP] = useState("");
    const [nameUpDel, setNameUpDel] = useState("");
    const [precioUpDel, setPreciopDel] = useState("");
    const [imageUpDel, setImagepDel] = useState(null); 
  
    const onClickDeleteDates = async (data) => {
        console.log(data);
        setIdUpDel(data.id);
        setIdP(data.id);  
        setNameUpDel(data.nombre);  
        setPreciopDel(data.precio);  
        setImagepDel(data.image);  
        handleShowEli();
      }
    const onClickDelete = async () => {
      setShoEliw(false)
      const datapost = { idUpDel, idP,nameUpDel,precioUpDel,imageUpDel}
  
      const res = await platillosDescativate.desactivate(datapost);
  
      if (res) {
        alert("Se Elimino");
        window.location.href = "/platillos";
  
      }
      else {
        alert("Se Elimino");
       window.location.href = "/platillos";
      }
    
      
    }
    //Modals
      const [show, setShow] = useState(false);
  
      const handleClose = () =>  setShow(false);
      const handleShow = () => setShow(true);
  
      const [showEli, setShoEliw] = useState(false);
  
      const handleCloseEli = () => setShoEliw(false);
      const handleShowEli = () => setShoEliw(true);
     
      const [showEdit, setShoEdit] = useState(false);
  
      const handleCloseEdit = () => setShoEdit(false);
      const handleShowEdit = () => setShoEdit(true);
  
      //Style para tablas
      const style = {
        height:'500px',display:'block',overflow:'scroll', width:'2000px'
      };
      return (
          <>  
          <div >
          <div className="container text-center col-md-11">
  
              <div className="card col-md-12  row justify-content-center m-3 mt-5">
                  <h1>Platillos</h1>
              </div>
             
          </div>
          <div className="container  col-md-11">
  
              <div className=" col-md-12  row  m-3 ">
                  <Button variant="primary btn btn-primary col-md-1" onClick={handleShow}>
                  
                      <i className="fa fa-fw fa-plus" style={{ fontSize: '1.75em', color: '#ffffff' }} />
  
                  </Button>
              </div>
               
          
          </div>
          <div className="container  col-md-11">
              <div className="container  mt-5 col-md-11">
              <table className="table col-md-12" >
              <thead className="bg-gray">
                  <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Foto</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Acciónes</th>
                  </tr>
              </thead>
              <tbody >
              {
                listPlatillos.map((item)=>{
                  return(
                    <tr>
                      <td>{item.nombre}</td>
                      <td><img src={item.foto} alt="lang.svg" width="150px" height="150px"/></td>
                      <td>{item.precio}</td>
                      <td>
                        <a onClick={() => onClickUpdateDates(item)} class="btn btn-info m-2 text-white"> Modificar </a>
                        <a onClick={() => onClickDeleteDates(item)} class="btn btn-danger m-2 text-white"> Desactivar </a>
                      </td>
                    </tr>
                  )
                })
              }
              </tbody>
              </table>
              </div>
          </div>
      </div>
  
      <Modal show={show} onHide={handleClose} size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
          <Modal.Header closeButton className="bg-gray text-center">
            <Modal.Title className="bg-gray text-center">Agregar platillos</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Container>
          <form >
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Nombre:{" "}
              <input type="text" 
              onChange={(e) => setName(e.target.value)}
              name="nombre"
              className="form-control"
              autoComplete="off"
              aria-describedby="emailHelp" />
              </label>
              </div>
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Foto:{" "}
              <input type="file" 
              onChange={(e) => setImage(e.target.files[0])}
              name="image"
              className="form-control"
              autoComplete="off"
              aria-describedby="image" />
              </label>
              </div>
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Precio:{" "}
              <input type="number" 
              onChange={(e) => setPrecio(e.target.value)}
              name="precio"
              className="form-control"
              autoComplete="off"
              aria-describedby="precio" />
              </label>
              </div>
        </form>
          </Container>
          </Modal.Body>
          <Modal.Footer>
            
            <Button variant="primary" onClick={()=>onClickSave()}>
              Guardar
            </Button>
            <Button variant="secondary" className="btn btn-danger"onClick={handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
      </Modal>
      
      <Modal show={showEli} onHide={handleCloseEli} size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
          <Modal.Header closeButton className="bg-gray text-center">
            <Modal.Title className="bg-gray text-center">Eliminar platillos</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Container>
              <h4>Seguro que desea eliminar el platillos?</h4>
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setIdUpDel(e.target.value)}
              name="id"
              value={idUp} />
              </label>
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setPreciopDel(e.target.value)}
              name="id"
              value={idUp} />
              </label>
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setImagepDel(e.target.files[0])}
              name="id"
              value={idUp} />
              </label>
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setNameUpDel(e.target.value)}
              name="id"
              value={idUp} />
              </label>
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setIdP(e.target.value)}
              name="id"
              value={idUp} />
              </label>
          </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={onClickDelete}>
              Si
            </Button>
            <Button variant="secondary" className="btn btn-danger" onClick={handleCloseEli}>
              Cerrar
            </Button>
          </Modal.Footer>
      </Modal>
  
      <Modal show={showEdit} onHide={handleCloseEdit} size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
          <Modal.Header closeButton className="bg-gray text-center">
            <Modal.Title className="bg-gray text-center">Modificar platillos</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Container>
          <form >
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
              <input type="hidden" 
              onChange={(e) => setIdUp(e.target.value)}
              name="id"
              value={idUp} />
              </label>
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Nombre:{" "}
              <input type="text" 
              onChange={(e) => setNameUp(e.target.value)}
              name="nombre"
              value={nameUp}
              className="form-control"
              autoComplete="off"
              aria-describedby="emailHelp" />
              </label>
              
              </div>
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Foto:{" "}
              <input type="file" 
              onChange={(e) => setImagep(e.target.files[0])}
              name="imageUp"
              className="form-control"
              autoComplete="off"
              aria-describedby="image" />
              </label>
              </div>
              <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Precio:{" "}
              <input type="numberUp" 
              onChange={(e) => setPreciop(e.target.value)}
              name="precio"
              value={precioUp}
  
              className="form-control"
              autoComplete="off"
              aria-describedby="precio" />
              </label>
              </div>
        </form>
          </Container>
          </Modal.Body>
          <Modal.Footer>
            
            <Button variant="primary" onClick={onClickSaveDates}>
              Guardar
            </Button>
            <Button variant="secondary" className="btn btn-danger"onClick={handleCloseEdit}>
              Cerrar
            </Button>
          </Modal.Footer>
      </Modal>
      
      </>
      )
  };
  export default Modelo;
  