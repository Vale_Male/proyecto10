import React, {useState,useEffect,useCallback} from 'react'
import {Button,  Modal,Container,Row,Col, NavLink }  from 'react-bootstrap';
import platillos from './services/platillos/servicePlatilloIna';
import platillosDelete from './services/platillos/servicePlatilloDelete';
import platillosDescativate from './services/platillos/servicePlatilloActivate';
import Vista from '../components/Vista';
import Swal from 'sweetalert2';
import { Alert } from 'reactstrap';
import { BrowserRouter as Router, Switch, Route,Link,useHistory } from 'react-router-dom';


import NavbarRoute from '../components/NavbarRoute';
var $ = require('jquery');
$.DataTable = require('datatables.net');


const PlatillosPageListAIn = () => {
  const history = useHistory();

  const handlePlatillosAt = () =>{
    history.push("/platillosListaAc");
}
  //Datatable 
  const [loading, setLoading] = useState(true);
  let setTableBody = '';
  //Listas
  const [ listPlatillos, setListCustomer ] = useState([]);
  useEffect(()=>{
    async function fetchDataCustomer(){
      const res = await platillos.lista();
      console.log(res.data);
      setListCustomer(res.data)

      setLoading(false);

      $('#tableDivision').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
        }
      });
    }
    fetchDataCustomer();
  },[])

  
 //Modificar 
 const [nameUp, setNameUp] = useState("");
 const [precioUp, setPreciop] = useState("");
 const [imageUp, setImagep] = useState(null);
 const [idUp, setIdUp] = useState(null);

 const onClickUpdateDates = async (data) => {
  console.log(data);
  setNameUp(data.nombre);
  setPreciop(data.precio);
  setImagep(data.image);
  setIdUp(data.id);
  
    handleShowEdit();
  }
  const onClickSaveDates = async () => {
    if(nameUp && precioUp){
      setShoEdit(false)
    const datapost = { nameUp, imageUp, precioUp,idUp}

    const res = await platillosDelete.delete(datapost);

    if (res) {
      Swal.fire({
        position: 'center',
        title: 'Eliminado!.',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      });
      handlePlatillosAt();
      
     // window.location.href = "/platillosListaAc";

    }
    else {
      Swal.fire({
        position: 'center',
        title: '¡Modificado!.',
        text: 'Success',
        icon: 'warning',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaIn";
      });

      //window.location.href = "/platillosListaAc";
    }
    }else{
      Swal.fire({
        position: 'center',
        title: '¡Ha ocurrido un error! Revisa los que los campos esten correctos.',
        text: 'error',
        icon: 'warning',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaIn";
      });
    }
    
  }
  //Desactivar 
  const [idUpDel, setIdUpDel] = useState("");
  const [idP, setIdP] = useState("");
  const [nameUpDel, setNameUpDel] = useState("");
  const [precioUpDel, setPreciopDel] = useState("");
  const [imageUpDel, setImagepDel] = useState(null); 

  const onClickDeleteDates = async (data) => {
      console.log(data);
      setIdUpDel(data.id);
      setIdP(data.id);  
      setNameUpDel(data.nombre);  
      setPreciopDel(data.precio);  
      setImagepDel(data.image);  
      handleShowEli();
    }
  const onClickDelete = async () => {
    setShoEliw(false)
    const datapost = { idUpDel, idP,nameUpDel,precioUpDel,imageUpDel}

    const res = await platillosDescativate.activate(datapost);

    if (res) {
      Swal.fire({
        position: 'center',
        title: 'Activado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaIn";
      });
      //window.location.href = "/platillosListaAc";

    }
    else {
      Swal.fire({
        position: 'center',
        title: '¡Eliminado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaIn";
      });
      //window.location.href = "/platillosListaAc";
    }
  
    
  }
  //Modals
    const [show, setShow] = useState(false);

    const handleClose = () =>  setShow(false);
    const handleShow = () => setShow(true);

    const [showEli, setShoEliw] = useState(false);

    const handleCloseEli = () => setShoEliw(false);
    const handleShowEli = () => setShoEliw(true);
   
    const [showEdit, setShoEdit] = useState(false);

    const handleCloseEdit = () => setShoEdit(false);
    const handleShowEdit = () => setShoEdit(true);

    //Style para tablas
    const style = {
      height:'500px',display:'block',overflow:'scroll', width:'2000px'
    };
    if (loading) {
      console.log('Aqui 1')

      setTableBody = <tr><td colSpan="4" className="text-center"></td></tr>;
    } else {
      console.log('Aqui 2')

      setTableBody = listPlatillos.map((item) => {
        console.log(item)
        return (
          <tr key={item.id}>
            {/* <td>{item.id}</td> */}
            <td>{item.nombre}</td>
            <td>{item.precio}</td>
            <td><img src={item.foto} alt="lang.svg" width="150px" height="150px"/></td>
            <td>
              <a onClick={() => onClickDeleteDates(item)} class="btn btn-danger m-2 text-white"> Activar </a>
            </td>
          </tr>
        );
      });
    }
    return (
        <>
                <Vista />

                <div className="m-5 justify-content-center text-center">

                  <div className=" m-5  justify-content-center text-center">
                    <div className=" text-center">
                        <div className="card col-md-12  row justify-content-center m-3 mt-5">
                            <h1 className="m-3">Lista Platillos Inactivos</h1>
                        </div>
                    </div>
                    <div className="text-center " >
                    <div className="col-md-12  row justify-content-center m-3 mt-5">

                            <table className="display w-100 table-light " id="tableDivision">
                              {/* table table-bordered table-hover */}
                              <thead className="bg-success m-5">
                                <tr>
                                  {/* <th scope="col">ID</th> */}
                                  <th scope="col" >Nombre </th>
                                  <th scope="col">Precio </th>
                                  <th scope="col">Foto </th>
                                  <th scope="col">Acciones </th>
                                </tr>
                              </thead>
                              <tbody>
                                {setTableBody}
                              </tbody>
                            </table>
                            </div>
                            </div>
                  </div>
                </div>

    <Modal show={showEli} onHide={handleCloseEli} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Eliminar platillos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
            <h4>Seguro que desea eliminar el platillos?</h4>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdUpDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setPreciopDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setImagepDel(e.target.files[0])}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setNameUpDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdP(e.target.value)}
            name="id"
            value={idUp} />
            </label>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={onClickDelete}>
            Si
          </Button>
          <Button variant="secondary" className="btn btn-danger" onClick={handleCloseEli}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>

    <Modal show={showEdit} onHide={handleCloseEdit} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Modificar platillos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
        <h4>Seguro que desea eliminar el platillo?</h4>

        <form >
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdUp(e.target.value)}
            name="id"
            value={idUp} />
            </label>
        </form>
      
        </Container>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="primary" onClick={onClickSaveDates}>
            Guardar
          </Button>
          <Button variant="secondary" className="btn btn-danger"onClick={handleCloseEdit}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>
    
    </>
    )
}

export default PlatillosPageListAIn;