import React, {useState,useEffect,useCallback} from 'react'
import platillos from './services/platillos/servicePlatillo';
import platillosAdd from './services/platillos/servicePlatilloAdd';

import { Card, CardImg, CardBody,UncontrolledCarousel,FormGroup,Label, Input,FormText,Button } from 'reactstrap';
import Vista from '../components/Vista';

//const dataAxios = peticionAxiosAsync().then(console.log);

const PlatillosPageAgregar = () => {
  //Traer Carrusel
  const [ listPlatillos, setListCustomer ] = useState([]);
  useEffect(()=>{
    async function fetchDataCustomer(){
      const res = await platillos.list();
      console.log(res.data);
      setListCustomer(res.data)
    }
    fetchDataCustomer();
  },[])
  const colores = [] ; 
  listPlatillos.forEach(function (response){
    console.log(response);
    colores.push({
      src:response.foto,
    altText: response.nombre,
    caption: ""
    })
  });
  //Agregar 
  const [name, setName] = useState("");
  const [precio, setPrecio] = useState("");
  const [image, setImage] = useState(null);

  const onClickSave = async () => {
    if(image&& name && precio){
    const datapost = { name, image, precio}

    const res = await platillosAdd.save(datapost);

    if (res) {
      alert("Se agrego");
      window.location.href = "/platillosAgregar";

    }
    else {
      alert("Se agrego");
      window.location.href = "/platillosAgregar";
    }
    }else{
      alert("Escriba los valores")
    }
    
  }

    return (
        <>
      <Vista />
      <div className="m-5 justify-content-center text-center">

      <div className=" m-5  justify-content-center text-center">
        <div className=" text-center">

            <div className="card col-md-12  row justify-content-center m-3 mt-5">
                <h1 className="m-3">Agregar Platillos</h1>
            </div>
           
        </div>
        <div className=" text-center justify-content-center row mt-5 ">
        <Card className="col-md-3 m-5">
            <CardBody className="m-5">
              <UncontrolledCarousel items={colores} />
            </CardBody>
        </Card>
        <div className="col-md-1  row justify-content-center m-3 mt-5">
            </div>
        <Card className="  col-md-6">
        <form className="container m-5">
          <FormGroup className="col-md-10">
            <Label for="exampleEmail">Nombre</Label>
            <Input
              type="text" 
              onChange={(e) => setName(e.target.value)}
              name="nombre"
              placeholder="Escribe el nombre"
            />
          </FormGroup>
          <FormGroup className="col-md-10">
            <Label for="examplePassword">Precio</Label>
            <Input
              type="number" 
              onChange={(e) => setPrecio(e.target.value)}
              name="precio"
              placeholder="Precio"
              autoComplete="off"
            />
          </FormGroup>
          <FormGroup className="col-md-10">
            <Label for="examplePassword">Foto del platillo</Label>
            <Input
              type="file"
              onChange={(e) => setImage(e.target.files[0])}
              name="image"
              placeholder="Password"
              autoComplete="off"
            />
          </FormGroup>
          <Button color="primary"  onClick={()=>onClickSave()}>
            Agregar
          </Button>
        </form>
        </Card>  
        
        </div>
       
        </div>
        </div>
    </>
    )
}

export default PlatillosPageAgregar;