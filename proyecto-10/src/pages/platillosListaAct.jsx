import React, {useState,useEffect,useCallback} from 'react'
import {Button,  Modal,Container,Row,Col, NavLink }  from 'react-bootstrap';
import platillos from './services/platillos/servicePlatillo';
import platillosUpdate from './services/platillos/servicePlatilloUpdate';
import platillosDescativate from './services/platillos/servicePlatilloDesactivate';
import Vista from '../components/Vista';
import Swal from 'sweetalert2';
import { Alert } from 'reactstrap';
import ReactDOM from 'react-dom';

import { browserHistory } from 'react-router';
import { BrowserRouter as Router, Switch, Route,Link,Redirect,useHistory} from 'react-router-dom';
import NavbarRoute from '../components/NavbarRoute';
var $ = require('jquery');
$.DataTable = require('datatables.net');


const PlatillosPageListAc = () => {
  const history = useHistory();

  const handlePlatillosAt = () =>{
    history.push("/platillosListaAc");
}
  //Datatable 
  const [loading, setLoading] = useState(true);
  let setTableBody = '';
  //Listas
  const [ listPlatillos, setListCustomer ] = useState([]);
  useEffect(()=>{
    async function fetchDataCustomer(){
      const res = await platillos.list();
      console.log(res.data);
      setListCustomer(res.data)

      setLoading(false);

      $('#tableDivision').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
        }
      });
    }
    fetchDataCustomer();
  },[])

  
 //Modificar 
 const [nameUp, setNameUp] = useState("");
 const [precioUp, setPreciop] = useState("");
 const [imageUp, setImagep] = useState(null);
 const [idUp, setIdUp] = useState(null);

 const onClickUpdateDates = async (data) => {
  console.log(data);
  setNameUp(data.nombre);
  setPreciop(data.precio);
  setImagep(data.image);
  setIdUp(data.id);
  
    handleShowEdit();
  }
  const onClickSaveDates = async () => {
    if(nameUp && precioUp){
      setShoEdit(false)
    const datapost = { nameUp, imageUp, precioUp,idUp}

    const res = await platillosUpdate.update(datapost);

    if (res) {
      Swal.fire({
        position: 'center',
        title: '¡Modificado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaAc";
      });
      //history.back();
     // window.location.href = "/platillosListaAc";
    }
    else {
      Swal.fire({
        position: 'center',
        title: '¡Modificado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaAc";
      });

              //window.location.href = "/platillosListaAc";
    }
    }else{
      Swal.fire({
        position: 'center',
        title: '¡Ha ocurrido un error! Revisa los que los campos esten correctos.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaAc";
      });
    }
    
  }
  //Desactivar 
  const [idUpDel, setIdUpDel] = useState("");
  const [idP, setIdP] = useState("");
  const [nameUpDel, setNameUpDel] = useState("");
  const [precioUpDel, setPreciopDel] = useState("");
  const [imageUpDel, setImagepDel] = useState(null); 

  const onClickDeleteDates = async (data) => {
      console.log(data);
      setIdUpDel(data.id);
      setIdP(data.id);  
      setNameUpDel(data.nombre);  
      setPreciopDel(data.precio);  
      setImagepDel(data.image);  
      handleShowEli();
    }
  const onClickDelete = async () => {
    setShoEliw(false)
    const datapost = { idUpDel, idP,nameUpDel,precioUpDel,imageUpDel}

    const res = await platillosDescativate.desactivate(datapost);

    if (res) {
      Swal.fire({
        position: 'center',
        title: '¡Eliminado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaAc";
      });
      //window.location.href = "/platillosListaAc";

    }
    else {
      Swal.fire({
        position: 'center',
        title: '¡Eliminado!.',
        icon: 'Success',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(function() {
        window.location = "/platillosListaAc";
      });
      //window.location.href = "/platillosListaAc";
    }
  
    
  }
  //Modals
    const [show, setShow] = useState(false);

    const handleClose = () =>  setShow(false);
    const handleShow = () => setShow(true);

    const [showEli, setShoEliw] = useState(false);

    const handleCloseEli = () => setShoEliw(false);
    const handleShowEli = () => setShoEliw(true);
   
    const [showEdit, setShoEdit] = useState(false);

    const handleCloseEdit = () => setShoEdit(false);
    const handleShowEdit = () => setShoEdit(true);

    //Style para tablas
    const style = {
      height:'500px',display:'block',overflow:'scroll', width:'2000px'
    };
    if (loading) {

      setTableBody = <tr><td colSpan="4" className="text-center"></td></tr>;
    } else {

      setTableBody = listPlatillos.map((item) => {
        console.log(item)
        return (
          <tr key={item.id}>
            {/* <td>{item.id}</td> */}
            <td>{item.nombre}</td>
            <td>{item.precio}</td>
            <td><img src={item.foto} alt="lang.svg" width="150px" height="150px"/></td>
            <td>
            <a onClick={() => onClickUpdateDates(item)} class="btn btn-info m-2 text-white"> Modificar </a>
            <a onClick={() => onClickDeleteDates(item)} class="btn btn-danger m-2 text-white"> Desactivar </a>
            </td>
          </tr>
        );
      });
    }
    return (
        <>
                <Vista />

                <div className="m-5 justify-content-center text-center">

                  <div className=" m-5  justify-content-center text-center">
                    <div className=" text-center">
                        <div className="card col-md-12  row justify-content-center m-3 mt-5">
                            <h1 className="m-3">Lista Platillos Activos</h1>
                        </div>
                    </div>
                    <div className="text-center " >
                    <div className="col-md-12  row justify-content-center m-3 mt-5">

                            <table className="display w-100 table-light " id="tableDivision">
                              {/* table table-bordered table-hover */}
                              <thead className="bg-success m-5">
                                <tr>
                                  {/* <th scope="col">ID</th> */}
                                  <th scope="col" >Nombre </th>
                                  <th scope="col">Precio </th>
                                  <th scope="col">Foto </th>
                                  <th scope="col">Acciones </th>
                                </tr>
                              </thead>
                              <tbody>
                                {setTableBody}
                              </tbody>
                            </table>
                            </div>
                            </div>
                  </div>
                </div>

    <Modal show={showEli} onHide={handleCloseEli} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Eliminar platillos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
            <h4>Seguro que desea desactivar el platillo?</h4>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdUpDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setPreciopDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setImagepDel(e.target.files[0])}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setNameUpDel(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdP(e.target.value)}
            name="id"
            value={idUp} />
            </label>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={onClickDelete}>
            Si
          </Button>
          <Button variant="secondary" className="btn btn-danger" onClick={handleCloseEli}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>

    <Modal show={showEdit} onHide={handleCloseEdit} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Modificar platillos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
        <form >
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12">
            <input type="hidden" 
            onChange={(e) => setIdUp(e.target.value)}
            name="id"
            value={idUp} />
            </label>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Nombre:{" "}
            <input type="text" 
            onChange={(e) => setNameUp(e.target.value)}
            name="nombre"
            value={nameUp}
            className="form-control"
            autoComplete="off"
            aria-describedby="emailHelp" />
            </label>
            
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Foto:{" "}
            <input type="file" 
            onChange={(e) => setImagep(e.target.files[0])}
            name="imageUp"
            className="form-control"
            autoComplete="off"
            aria-describedby="image" />
            </label>
            </div>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label col-md-12"> Precio:{" "}
            <input type="numberUp" 
            onChange={(e) => setPreciop(e.target.value)}
            name="precio"
            value={precioUp}

            className="form-control"
            autoComplete="off"
            aria-describedby="precio" />
            </label>
            </div>
      </form>
      
        </Container>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="primary" onClick={onClickSaveDates}>
            Guardar
          </Button>
          <Button variant="secondary" className="btn btn-danger"onClick={handleCloseEdit}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>
    
    </>
    )
}

export default PlatillosPageListAc;