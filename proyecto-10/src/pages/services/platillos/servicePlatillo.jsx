import axios from "axios";
const baseUrl = "https://apis.pegasoirapuato.com/public/api/v1/posts";
const platillos = {};

/* ...*/

platillos.list = async () => {
  const urlList = baseUrl+""
  const res = await axios.get(urlList)
  .then(response=> {return response.data })
  .catch(error=>{ return error; })
  return res;
}

export default platillos