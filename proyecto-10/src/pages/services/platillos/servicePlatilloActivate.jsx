import axios from "axios";

const platillos = {};

platillos.activate = async (data) => {
    let formData = new FormData();
    formData.append('precio',data.precioUpDel)
    formData.append('nombre', data.nameUpDel)
    formData.append('image', data.imageUpDel)
    formData.append('act', data.idP)
    formData.append('id', data.idUpDel)
    formData.append('_method', 'PUT')
    console.log(formData);
    
    axios({
        method: "post",
        url:"https://apis.pegasoirapuato.com/public/api/v1/posts/"+data.idUp ,
        data: formData,
        headers: { "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "Authorization": "Bearer",
        "type": "formData"
    },
      })
        .then(function (response) {
          //handle success
          console.log(response);
        })
        .catch(function (response) {
          //handle error
          console.log(response);
        });
}


export default platillos