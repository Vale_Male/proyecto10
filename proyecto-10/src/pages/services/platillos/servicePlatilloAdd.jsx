import axios from "axios";

const customer = {};

customer.save = async (data) => {

    let formData = new FormData();
    formData.append('precio',data.precio)
    formData.append('nombre', data.name)
    formData.append('image', data.image)
    console.log(formData);
    
    axios({
        method: "post",
        url:"https://apis.pegasoirapuato.com/public/api/v1/posts" ,
        data: formData,
        headers: { "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "Authorization": "Bearer",
        "type": "formData"
    },
      })
        .then(function (response) {
          //handle success
          console.log(response);
        })
        .catch(function (response) {
          //handle error
          console.log(response);
        });
}


export default customer