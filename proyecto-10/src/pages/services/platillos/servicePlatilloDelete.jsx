import axios from "axios";

const platillos = {};

platillos.delete = async (data) => {

    let formData = new FormData();
    formData.append('precio',data.precioUp)
    formData.append('nombre', data.nameUp)
    formData.append('image', data.imageUp)
    formData.append('id', data.idUp)
    console.log(formData);
    
    axios({
        method: "delete",
        url:"https://apis.pegasoirapuato.com/public/api/v1/posts/"+data.idUp ,
        //url:"http://127.0.0.1:8000/api/v1/posts/"+data.idUp ,
        //
        data: formData,
        headers: { "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "Authorization": "Bearer",
        "type": "formData"
    },
      })
        .then(function (response) {
          //handle success
          console.log(response);
        })
        .catch(function (response) {
          //handle error
          console.log(response);
        });
}


export default platillos