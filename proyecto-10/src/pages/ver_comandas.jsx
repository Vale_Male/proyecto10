import React, {useState} from 'react'
import {Button,  Modal,Container,Row,Col }  from 'react-bootstrap';
import NavbarRoute from '../components/NavbarRoute';

const VistaComandasPage = () => {
    
    const [showEdit, setShoEdit] = useState(false);

    const handleCloseEdit = () => setShoEdit(false);
    const handleShowEdit = () => setShoEdit(true);
    return (
        <>
                <NavbarRoute />

        <div>
        <div className="container text-center col-md-11">

            <div className="card col-md-12  row justify-content-center m-3 mt-5">
                <h1>Ver comandas</h1>
            </div>
           
        </div>
        <div className="container  col-md-11">
        </div>
        <div className="container  col-md-11">
            <div className="container  mt-5 col-md-11">
            <table className="table">
            <thead className="bg-gray">
                <tr>
                <th scope="col">No. comandas</th>
                <th scope="col">Platillo</th>
                <th scope="col">Nota</th>
                <th scope="col">Total</th>
                <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example"  >
                        <button type="button" className="btn btn-info" onClick={handleShowEdit}>
                            <i className="fa fa-fw fa-money-bill-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>
                </tr>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example"  >
                        <button type="button" className="btn btn-info" onClick={handleShowEdit}>
                            <i className="fa fa-fw fa-money-bill-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>
                </tr>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                <td>
                    <div className="btn-group" role="group" aria-label="Basic example"  >
                        <button type="button" className="btn btn-info" onClick={handleShowEdit}>
                            <i className="fa fa-fw fa-money-bill-alt" style={{ fontSize: '1.75em', color: '#ffffff' }} />
                        </button>
                    </div>
                </td>
                </tr>
            </tbody>
            </table>
            </div>
        </div>
    </div>
    
    <Modal show={showEdit} onHide={handleCloseEdit} size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
        <Modal.Header closeButton className="bg-gray text-center">
          <Modal.Title className="bg-gray text-center">Cuenta</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Container>
        <table className="table">
            <thead className="bg-light">
                <tr>
                <th scope="col">Platillo</th>
                <th scope="col">Precio</th>
                <th scope="col">Cantidad</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>                
                </tr>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                </tr>
                <tr>
                <th >Item 1</th>
                <th >Item 1</th>
                <th >Item 1</th>
                </tr>
            </tbody>
            </table>
            <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">Total</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            </div>
        </Container>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="success" onClick={handleCloseEdit}>
            Pagar
          </Button>
          <Button variant="secondary" className="btn btn-danger"onClick={handleCloseEdit}>
            Cerrar
          </Button>
        </Modal.Footer>
    </Modal>
    </>
    )
}

export default VistaComandasPage;